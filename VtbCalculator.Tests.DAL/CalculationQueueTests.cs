﻿using System.Linq;
using VtbCalculator.DAL.Repositories;
using VtbCalculator.Domain.Models;
using NUnit.Framework;

namespace VtbCalculator.Tests.DAL
{
    [TestFixture]
    public class CalculationQueueTests
    {
        [Test]
        public void AddGetDeleteCalculationRequest()
        {
            const OperationType addition = OperationType.Addition;
            const int operandLeft = 3;
            const int operandRight = 5;

            var repo = new CalculationQueueRepository();
            repo.Add(new CalculationRequest
            {
                OperationType = addition,
                OperandLeft = operandLeft,
                OperandRight = operandRight
            });

            var gottenRequest = repo.GetAll().Single(x => x.OperationType == addition && x.OperandLeft == operandLeft &&
                                                          x.OperandRight == operandRight);
            Assert.AreEqual(gottenRequest.OperationType, addition);
            Assert.AreEqual(gottenRequest.OperandLeft, operandLeft);
            Assert.AreEqual(gottenRequest.OperandRight, operandRight);

            repo.Remove(gottenRequest.Id);
            Assert.IsFalse(repo.GetAll().Any());
        }
    }
}