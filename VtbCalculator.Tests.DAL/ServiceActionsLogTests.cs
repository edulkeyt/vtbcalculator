﻿using System;
using System.Linq;
using VtbCalculator.DAL.Repositories;
using VtbCalculator.Domain.Models;
using NUnit.Framework;

namespace VtbCalculator.Tests.DAL
{
    [TestFixture]
    public class ServiceActionsLogTests
    {
        [Test]
        public void AddGetLogEntryTest()
        {
            var time = DateTime.UtcNow;
            const string Source = "tstSource";
            const string Action = "tstAction";
            const string Message = "tstMessage";

            var repo = new ServiceActionsLogRepository();
            repo.Write(new LogEntry() { Action = Action, Message = Message, Source = Source, Time = time });

            var entries = repo.GetInTimeRange(DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(30)), DateTime.UtcNow);
            Assert.IsTrue(entries.Any(x => x.Time == time && x.Message == Message && x.Action == Action &&
                                    x.Source == Source));
        }
    }
}
