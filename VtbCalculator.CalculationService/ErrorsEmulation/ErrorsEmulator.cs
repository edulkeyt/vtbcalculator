﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace VtbCalculator.CalculationService.ErrorsEmulation
{
    public static class ErrorsEmulator
    {
        private static Random rand = new Random();

        public static void EmulateTimeoutExcess(double probability, int timeoutSeconds)
        {
            if (CheckProbability(probability))
            {
                Thread.Sleep(TimeSpan.FromSeconds(timeoutSeconds));
            }
        }

        private static bool CheckProbability(double probability)
        {
            return rand.Next(101) / 100.0 <= probability;
        }
    }
}