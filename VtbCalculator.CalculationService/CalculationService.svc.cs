﻿using VtbCalculator.BusinessLogic.Models;
using VtbCalculator.DbLogger;

namespace VtbCalculator.CalculationService
{
    public class CalculationService : ICalculationService
    {
        private readonly BusinessLogic.Calculation.ICalculationService _calculationService =
            new BusinessLogic.Calculation.CalculationService();

        private readonly IDbLogger _dbLogger = new DbLogger.DbLogger();

        public CalculationResultModel Calculate(CalculationRequestModel calculationRequest)
        {
            _dbLogger.Log(nameof(CalculationService), nameof(Calculate),
                $"calculation request received. Operation id:{calculationRequest.OperationType}, left operand:{calculationRequest.LeftOperand}, right operand:{calculationRequest.RightOperand}");

            ErrorsEmulation.ErrorsEmulator.EmulateTimeoutExcess(0.33, 670);

            var result = _calculationService.Calculate(calculationRequest);

            _dbLogger.Log(nameof(CalculationService), nameof(Calculate),
                $"Start to send calculation result. Operation id:{calculationRequest.OperationType}, left operand:{calculationRequest.LeftOperand}, right operand:{calculationRequest.RightOperand}. Result isSuccess:{result.IsSuccessfull}, resultValue{result.Result}, message:{result.Message}");

            return result;
        }
    }
}