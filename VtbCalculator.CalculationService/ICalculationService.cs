﻿using System.ServiceModel;
using VtbCalculator.BusinessLogic.Models;

namespace VtbCalculator.CalculationService
{
    [ServiceContract]
    public interface ICalculationService
    {
        [OperationContract]
        CalculationResultModel Calculate(CalculationRequestModel calculationRequest);
    }
}